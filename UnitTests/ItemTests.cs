﻿using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;
using Item = Farmory.Item;

namespace UnitTests
{
	class ItemTests
	{
		public static void RunTests()
		{
			WebRequest request = WebRequest.Create("http://www.battle.net/api/wow/item/18803");
			Stream stream = request.GetResponse().GetResponseStream();

			DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(Item));
			Item item = serializer.ReadObject(stream) as Item;
		}
	}
}
