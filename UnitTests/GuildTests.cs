﻿using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;

using Guild = Farmory.Guild;

namespace UnitTests
{
	class GuildTests
	{
		public static void RunTests()
		{
			WebRequest request = WebRequest.Create("http://www.battle.net/api/wow/guild/garithos/Dragon Flight?fields=members");
			Stream stream = request.GetResponse().GetResponseStream();

			DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(Guild));
			Guild guild = serializer.ReadObject(stream) as Guild;
		}
	}
}
