﻿using System.Runtime.Serialization;
using System.Collections.Generic;
using System.IO;

namespace Farmory
{
	[DataContract]
    class TalentData
    {
		[DataContract]
		public class TalentGlyph
		{
			[DataContract]
			class Glyph
			{
				[DataMember(Name = "glyph")]
				int id;

				[DataMember(Name = "item")]
				int item;

				[DataMember(Name = "name")]
				string name;

				[DataMember(Name = "icon")]
				string icon;

				[DataMember(Name = "typeId")]
				int typeId;
			}

			[DataMember(Name = "glyphs")]
			List<Glyph> glyphs;

			[DataContract]
			public class Talent
			{
				[DataMember(Name = "tier")]
				public int tier;

				[DataMember(Name = "column")]
				public int column;

				[DataContract]
				public class Spell
				{
					[DataMember(Name = "id")]
					public int id;

					[DataMember(Name = "name")]
					public string name;

					[DataMember(Name = "icon")]
					public string icon;

					[DataMember(Name = "description")]
					public string description;

					[DataMember(Name = "castTime")]
					public string castTime;
				}

				[DataMember(Name = "spell")]
				public Spell spell;
			}

			[DataMember(Name = "talents")]
			public List<List<Talent>> talents;
		}

		[DataMember(Name = "1")]
		TalentGlyph warrior;

		[DataMember(Name = "2")]
		TalentGlyph paladin;

		[DataMember(Name = "3")]
		TalentGlyph hunter;

		[DataMember(Name = "4")]
		TalentGlyph rogue;

		[DataMember(Name = "5")]
		TalentGlyph priest;

		[DataMember(Name = "6")]
		TalentGlyph deathknight;

		[DataMember(Name = "7")]
		TalentGlyph shaman;

		[DataMember(Name = "8")]
		TalentGlyph mage;

		[DataMember(Name = "9")]
		TalentGlyph warlock;

		[DataMember(Name = "10")]
		TalentGlyph monk;

		[DataMember(Name = "11")]
		TalentGlyph druid;

		public void PrintQuery(StreamWriter stream)
		{
			PrintClassQuery(stream, warrior);
			PrintClassQuery(stream, paladin);
			PrintClassQuery(stream, hunter);
			PrintClassQuery(stream, rogue);
			PrintClassQuery(stream, priest);
			PrintClassQuery(stream, deathknight);
			PrintClassQuery(stream, shaman);
			PrintClassQuery(stream, mage);
			PrintClassQuery(stream, warlock);
			PrintClassQuery(stream, monk);
			PrintClassQuery(stream, druid);
		}

		private void PrintClassQuery(StreamWriter stream, TalentGlyph c)
		{
			for (int tier = 0; tier < c.talents.Count; tier++)
			{
				for (int column = 0; column < c.talents[tier].Count; column++)
				{
					TalentGlyph.Talent t = c.talents[tier][column];
					stream.WriteLine("INSERT INTO Talents VALUES (" + t.spell.id + ",\"" + t.spell.name + "\",\"" + t.spell.description + "\","
										+ t.column + "," +  t.tier + ");");
				}
			}
		}
    }
}
