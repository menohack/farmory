﻿using System.Runtime.Serialization;
using System.Collections.Generic;
using System.IO;

namespace Farmory
{
	[DataContract]
	public class Guild
	{
		
		[DataMember(Name = "name")]
		string name;
		
		[DataMember(Name = "level")]
		int level;

		[DataMember(Name = "realm")]
		string realm;

		[DataMember(Name = "battlegroup")]
		string battlegroup;

		[DataMember(Name = "side")]
		int side;

		[DataMember(Name = "achievementPoints")]
		int achievementPoints;

		[DataMember(Name = "lastModified")]
		long lastModified;

		[DataContract]
		public class Member
		{

			[DataContract]
			public class Churdur
			{
				[DataMember(Name = "name")]
				public string name;

				[DataMember(Name = "realm")]
				public string realm;
			}

			[DataMember(Name = "character")]
			public Churdur character;

			//[DataMember(Name = "character")]
			//Character character;

			[DataMember(Name = "rank")]
			int rank;
		}

		[DataMember(Name = "members")]
		public List<Member> members;

		public int GID = -1;

		public bool PrintQuery(StreamWriter stream)
		{
			if (!IsFinished())
				return false;

			stream.WriteLine("INSERT INTO Guilds VALUES (" + GID + ",\"" + name + "\"," + level + ",\"" + realm + "\",\""
					+ battlegroup + "\"," + side + "," + achievementPoints + ");");

			return true;
		}

		public bool IsFinished()
		{
			return GID != -1;
		}
	}
}
