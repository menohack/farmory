﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;

namespace Farmory
{
	[DataContract]
	public class Character
	{
		[DataMember(Name = "achievementPoints")]
		public int achievementPoints;

		[DataContract]
		public class Achievements
		{
			[DataMember(Name = "achievementsCompleted")]
			public List<int> achievementsCompleted;

			[DataMember(Name = "achievementsCompletedTimestamp")]
			public List<long> achievementsCompletedTimestamp;

			[DataMember(Name = "criteria")]
			public List<int> criteria;

			[DataMember(Name = "criteriaCreated")]
			public List<long> criteriaCreated;

			[DataMember(Name = "criteriaQuantity")]
			public List<long> criteriaQuantity;

			[DataMember(Name = "criteriaTimestamp")]
			public List<long> criteriaTimestamp;
		}

		[DataMember(Name = "achievements")]
		public Achievements achievements;

		[DataMember(Name = "battlegroup")]
		public string battlegroup;

		[DataMember(Name = "calcClass")]
		public string calcClass;

		[DataMember(Name = "class")]
		public int classId;

		[DataMember(Name = "gender")]
		public int genderId;

		[DataMember(Name = "lastModified")]
		public long lastModified;

		[DataMember(Name = "level")]
		public int level;

		[DataMember(Name = "name")]
		public string name;

		[DataMember(Name = "race")]
		public int race;

		[DataMember(Name = "realm")]
		public string realm;

		[DataMember(Name = "thumbnail")]
		public string thumbnail;

		[DataContract]
		public class Items
		{
			[DataMember(Name = "averageItemLevel")]
			public string averageItemLevel;

			[DataContract]
			public class Item
			{
				[DataMember(Name = "id")]
				public int IID;

				[DataMember(Name = "name")]
				string name;

				[DataMember(Name = "quality")]
				int quality;
			}

			[DataMember(Name = "back")]
			public Item back;

			[DataMember(Name = "chest")]
			public Item chest;

			[DataMember(Name = "feet")]
			public Item feet;

			[DataMember(Name = "finger1")]
			public Item finger1;

			[DataMember(Name = "finger2")]
			public Item finger2;

			[DataMember(Name = "hands")]
			public Item hands;

			[DataMember(Name = "legs")]
			public Item legs;

			[DataMember(Name = "mainHand")]
			public Item mainHand;

			[DataMember(Name = "offHand")]
			public Item offHand;

			[DataMember(Name = "neck")]
			public Item neck;

			[DataMember(Name = "shoulder")]
			public Item shoulder;

			[DataMember(Name = "trinket1")]
			public Item trinket1;

			[DataMember(Name = "trinket2")]
			public Item trinket2;

			[DataMember(Name = "waist")]
			public Item waist;

			[DataMember(Name = "wrist")]
			public Item wrist;

		}

		[DataMember(Name = "items")]
		public Items items;

		[DataContract]
		public class Guild
		{
			[DataMember(Name = "name")]
			public string name;

			[DataMember(Name = "members")]
			public int members;

			[DataMember(Name = "realm")]
			public string realm;
		}

		[DataMember(Name = "guild")]
		public Guild guild;

		[DataContract]
		public class Talent1
		{
			[DataMember(Name = "calcGlyph")]
			string calcGlyph;

			[DataMember(Name = "calcSpec")]
			string calcSpec;

			[DataMember(Name = "calcTalent")]
			string calcTalent;

			[DataContract]
			public class Glyphs
			{

				[DataContract]
				public class Glyph
				{
					[DataMember(Name = "glyph")]
					int glyph;

					[DataMember(Name = "item")]
					int item;

					[DataMember(Name = "name")]
					string name;
				}

				[DataMember(Name = "major")]
				List<Glyph> major;

				[DataMember(Name = "minor")]
				List<Glyph> minor;
			}

			[DataMember(Name = "glyphs")]
			Glyphs glyphs;

			[DataMember(Name = "selected")]
			bool selected;

			[DataContract]
			public class Spec
			{
				[DataMember(Name = "description")]
				string description;

				[DataMember(Name = "name")]
				string name;

				[DataMember(Name = "order")]
				int order;

				[DataMember(Name = "role")]
				string role;
			}

			[DataMember(Name = "spec")]
			Spec spec;

			[DataContract]
			public class Talent2
			{
				[DataMember]
				int column;

				[DataContract]
				public class Spell
				{
					[DataMember(Name = "castTime")]
					string castTime;

					[DataMember(Name = "description")]
					string description;

					[DataMember(Name = "id")]
					public int id;

					[DataMember(Name = "name")]
					string name;

					[DataMember(Name = "subtext")]
					string subtext;
				}

				[DataMember(Name = "spell")]
				public Spell spell;

				[DataMember(Name = "tier")]
				int tier;
			}

			[DataMember(Name = "talents")]
			public List<Talent2> talents;

		}

		[DataMember(Name = "talents")]
		List<Talent1> talents;

		public int GID = -1;

		public int PID = -1;

		/// <summary>
		/// Write the mysql statement that inserts this object into the database.
		/// </summary>
		/// <param name="stream">The stream to which the statement will be written.</param>
		/// <returns>True if the statement was written, false otherwise.</returns>
		public bool PrintQuery(StreamWriter stream)
		{
			if (!IsFinished())
				return false;
			
			//Write the Player entry
			stream.WriteLine("INSERT INTO Players VALUES (" + PID + ",\"" + name + "\"," + level + "," + GID + ",\""
					+ battlegroup + "\",\"" + TranslateClassId(classId) + "\",\"" + TranslateGenderId(genderId) + "\",\""
					+ TranslateRaceId(race) + "\",\"" + realm + "\"," + items.averageItemLevel + "," + lastModified
					+ "," + achievementPoints + ");");

			//Write the achievements earned
			for (int i = 0; i < achievements.achievementsCompleted.Count && i < achievements.achievementsCompletedTimestamp.Count; i++)
				stream.WriteLine("INSERT INTO PlayerAchieved VALUES (" + achievements.achievementsCompleted[i] 
					+ "," + PID + "," + achievements.achievementsCompletedTimestamp[i] + ");");

			//Write the talents
			foreach (Talent1 t1 in talents)
				foreach (Talent1.Talent2 t2 in t1.talents)
					stream.WriteLine("INSERT INTO HasTalents VALUES ({0},{1});", t2.spell.id, PID);

			//Write the equipped items
			if (items != null)
			{
				if (items.back != null)
					stream.WriteLine("INSERT INTO Equipping VALUES ({0},{1},\"{2}\");", PID, items.back.IID, "back");
				if (items.chest != null)
					stream.WriteLine("INSERT INTO Equipping VALUES ({0},{1},\"{2}\");", PID, items.chest.IID, "chest");
				if (items.feet != null)
					stream.WriteLine("INSERT INTO Equipping VALUES ({0},{1},\"{2}\");", PID, items.feet.IID, "feet");
				if (items.finger1 != null)
					stream.WriteLine("INSERT INTO Equipping VALUES ({0},{1},\"{2}\");", PID, items.finger1.IID, "finger1");
				if (items.finger2 != null)
					stream.WriteLine("INSERT INTO Equipping VALUES ({0},{1},\"{2}\");", PID, items.finger2.IID, "finger2");
				if (items.hands != null)
					stream.WriteLine("INSERT INTO Equipping VALUES ({0},{1},\"{2}\");", PID, items.hands.IID, "hands");
				if (items.legs != null)
					stream.WriteLine("INSERT INTO Equipping VALUES ({0},{1},\"{2}\");", PID, items.legs.IID, "legs");
				if (items.mainHand != null)
					stream.WriteLine("INSERT INTO Equipping VALUES ({0},{1},\"{2}\");", PID, items.mainHand.IID, "mainHand");
				if (items.offHand != null)
					stream.WriteLine("INSERT INTO Equipping VALUES ({0},{1},\"{2}\");", PID, items.offHand.IID, "offHand");
				if (items.neck != null)
					stream.WriteLine("INSERT INTO Equipping VALUES ({0},{1},\"{2}\");", PID, items.neck.IID, "neck");
				if (items.shoulder != null)
					stream.WriteLine("INSERT INTO Equipping VALUES ({0},{1},\"{2}\");", PID, items.shoulder.IID, "shoulder");
				if (items.trinket1 != null)
					stream.WriteLine("INSERT INTO Equipping VALUES ({0},{1},\"{2}\");", PID, items.trinket1.IID, "trinket1");
				if (items.trinket2 != null)
					stream.WriteLine("INSERT INTO Equipping VALUES ({0},{1},\"{2}\");", PID, items.trinket2.IID, "trinket2");
				if (items.waist != null)
					stream.WriteLine("INSERT INTO Equipping VALUES ({0},{1},\"{2}\");", PID, items.waist.IID, "waist");
				if (items.wrist != null)
					stream.WriteLine("INSERT INTO Equipping VALUES ({0},{1},\"{2}\");", PID, items.wrist.IID, "wrist");
			}

			return true;
		}

		/// <summary>
		/// Returns true if the character is finished and ready to put into the database.
		/// </summary>
		/// <returns>True if finished, false otherwise.</returns>
		public bool IsFinished()
		{
			return GID != -1 && PID != -1;
		}

		/// <summary>
		/// Translates an integer class ID to the class name.
		/// </summary>
		/// <param name="classId">The class ID.</param>
		/// <returns>The name of the class.</returns>
		public static string TranslateClassId(int classId)
		{
			switch (classId)
			{
				case 3: 
					return "Hunter";
				case 4: 
					return "Rogue";
				case 1:
					return "Warrior";
				case 2:
					return "Paladin";
				case 7:
					return "Shaman";
				case 8:
					return "Mage";
				case 5:
					return "Priest";
				case 6:
					return "Death Knight";
				case 11:
					return "Druid";
				case 9:
					return "Warlock";
				case 10:
					return "Monk";
				default: 
					return "Error";
			}
		}

		/// <summary>
		/// Translates an integer ID into the race name.
		/// </summary>
		/// <param name="raceId">The race ID.</param>
		/// <returns>The name of the race.</returns>
		public static string TranslateRaceId(int raceId)
		{
			switch (raceId)
			{
				case 1:
					return "Human";
				case 5:
					return "Undead";
				case 11:
					return "Draenei";
				case 7:
					return "Gnome";
				case 8:
					return "Troll";
				case 2:
					return "Orc";
				case 3:
					return "Dwarf";
				case 4:
					return "Night Elf";
				case 10:
					return "Blood Elf";
				case 22:
					return "Worgen";
				case 6:
					return "Tauren";
				case 24:
				case 25:
				case 26:
					return "Pandaren";
				case 9:
					return "Goblin";
				default:
					return "Error";
			}
		}

		/// <summary>
		/// Translates the gender ID into 'M' or 'F'.
		/// </summary>
		/// <param name="genderId">The gender ID.</param>
		/// <returns>The character 'M' or 'F'.</returns>
		public static char TranslateGenderId(int genderId)
		{
			if (genderId == 0)
				return 'M';
			else
				return 'F';
		}
	}
}
