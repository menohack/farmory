﻿using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Farmory
{
	[DataContract]
	public class Challenge
	{
		[DataContract]
		public class ChallengeNode
		{
			[DataContract]
			public class ChallengeGroup
			{
				[DataMember(Name = "date")]
				public string date;

				[DataMember(Name = "faction")]
				public string faction;

				[DataMember(Name = "isRecurring")]
				public bool isRecurring;

				[DataMember(Name = "medal")]
				public string medal;

				[DataContract]
				public class ChallengeMember
				{
					[DataMember(Name = "character")]
					public Character character;

					[DataContract]
					public class ChallengeSpec
					{
						[DataMember(Name = "backgroundImage")]
						public string backgroundImage;

						[DataMember(Name = "description")]
						public string description;

						[DataMember(Name = "icon")]
						public string icon;

						[DataMember(Name = "name")]
						public string name;

						[DataMember(Name = "order")]
						public int order;

						[DataMember(Name = "role")]
						public string role;
					}

					[DataMember(Name = "spec")]
					public ChallengeSpec spec;
				}

				[DataMember(Name = "members")]
				public List<ChallengeMember> members;

				[DataMember(Name = "ranking")]
				public int ranking;
			}

			[DataMember(Name = "groups")]
			public List<ChallengeGroup> groups;
		}

		[DataMember(Name = "challenge")]
		public List<ChallengeNode> elements;
	}
}
