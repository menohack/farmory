﻿using System;
using System.Net;
using System.IO;
using System.Collections.Generic;
using System.Runtime.Serialization.Json;

namespace Farmory
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	class Farmory
	{
		public const String BASE_URL = "http://www.battle.net/api/wow/";

		public const String CHARACTER_FIELDS_URL_END = "?fields=guild,items,achievements,talents";

		/// <summary>
		/// The file name of a list of full-length http links and the time they were scraped in the format Date: link.
		/// </summary>
		public const String LOG_FILENAME = "scrapelog.log";

		/// <summary>
		/// The file name of a list of full-length http links that have not been scraped.
		/// </summary>
		public const String LINKS_QUEUE_FILENAME = "linksqueue.log";

		/// <summary>
		/// The raw output file name from each scrape.
		/// </summary>
		public const String OUTPUT_FILENAME = "output.json";

		/// <summary>
		/// The file name of the mysql queries file.
		/// </summary>
		public const String QUERIES_FILENAME = "queries.mysql";

		/// <summary>
		/// The file name of the list of guilds and their GIDs.
		/// </summary>
		public const String GUILDS_FILENAME = "guilds.log";

		public const String FAILED_LINKS_FILENAME = "failedlinks.log";

		/// <summary>
		/// The file name of the list of players and their PIDs.
		/// </summary>
		//public const String PLAYERS_FILENAME = "players.log";

		/// <summary>
		/// The file name of the list of items and their IIDs.
		/// </summary>
		//public const String ITEMS_FILENAME = "items.log";

		//public const String WEAPONS_FILENAME = "weapons.log";

		public const String NEXT_IDS_FILENAME = "nextids.log";

		/// <summary>
		/// The number of requests to the API for this session.
		/// </summary>
		private static int scrapeCount = 0;

		/// <summary>
		/// The next GID.
		/// </summary>
		public static int nextGID = -1;

		/// <summary>
		/// The list of guilds and their GIDs.
		/// </summary>
		public static Dictionary<string, int> guilds = new Dictionary<string, int>();

		/// <summary>
		/// The next PID.
		/// </summary>
		public static int nextPID = -1;

		/// <summary>
		/// The list of players and their PIDs;
		/// </summary>
		//public static Dictionary<string, int> players = new Dictionary<string, int>();

		/// <summary>
		/// The next IID;
		/// </summary>
		//public static int nextIID = -1;

		//public static Dictionary<string, int> items = new Dictionary<string, int>();

		/// <summary>
		/// The next WID.
		/// </summary>
		public static int nextWID = -1;

		//public static Dictionary<string, int> weapons = new Dictionary<string, int>();

		/// <summary>
		/// The list of http links that have already been scraped.
		/// Example: http://www.battle.net/api/wow/character/garithos/pericula?fields=guild,items,achievements,talents
		/// </summary>
		private static Dictionary<String, String> scrapedUrls = new Dictionary<String, String>();

		/// <summary>
		/// The list of http links that have not been scraped.
		/// Example: http://www.battle.net/api/wow/character/garithos/pericula?fields=guild,items,achievements,talents
		/// </summary>
		private static Dictionary<String, String> linksQueued = new Dictionary<String, String>();

		/// <summary>
		/// A list of characters that have been partially scraped (i.e. not from character/).
		/// </summary>
		//private static List<Character> unfinishedCharacters = new List<Character>();

		/// <summary>
		/// Performs an HTTP GET request of sURL. A log of the date and url is written to LOG_FILENAME
		/// and the result of the request is written to OUTPUT_FILENAME.
		/// </summary>
		/// <param name="urlEnd">The url.</param>
		public static Stream Scrape(String sURL)
		{
			scrapeCount++;
			//Open the GET request
			WebRequest wrGETURL = WebRequest.Create(sURL);
			Stream objStream = wrGETURL.GetResponse().GetResponseStream();
			StreamReader objReader = new StreamReader(objStream);

			//Open the ouput file stream
			FileStream fileStream = File.Open(OUTPUT_FILENAME, FileMode.Append);

			StreamWriter writer = new StreamWriter(fileStream);

			string result = objReader.ReadToEnd();
			writer.WriteLine(result);
			writer.Close();

			FileStream logStream = File.Open(LOG_FILENAME, FileMode.Append);

			StreamWriter logWriter = new StreamWriter(logStream);
			logWriter.WriteLine(DateTime.Now + ": " + sURL);
			logWriter.Close();

			//Add the link to scrapedUrls
			scrapedUrls.Add(sURL, sURL);


			MemoryStream output = new MemoryStream();
			StreamWriter outputWriter = new StreamWriter(output);
			outputWriter.Write(result);
			outputWriter.Flush();
			output.Position = 0;

			return output;
		}

		/// <summary>
		/// Loads the list of previously scraped urls from LOG_FILENAME into memory.
		/// </summary>
		static void LoadScrapedUrls()
		{
			//Open the log file
			FileStream logStream;
			try
			{
				logStream = File.OpenRead(LOG_FILENAME);
			}
			catch (FileNotFoundException)
			{
				return;
			}

			StreamReader reader = new StreamReader(logStream);

			//Read all of the lines in the log and extract the links into the dictionary
			string line = "";
			while ((line = reader.ReadLine()) != null)
			{
				int pos = line.IndexOf(BASE_URL);
				if (pos != -1)
				{

					string link = line.Substring(pos);
					scrapedUrls.Add(link, link);
				}
			}

			logStream.Close();
		}

		/// <summary>
		/// Load the list of unscraped links from LINKS_FILENAME.
		/// </summary>
		static void LoadLinksQueue()
		{
			//Open the log file
			FileStream logStream;
			try
			{
				logStream = File.OpenRead(LINKS_QUEUE_FILENAME);
			}
			catch (FileNotFoundException)
			{
				return;
			}

			StreamReader reader = new StreamReader(logStream);

			//Read all of the lines in the log and extract the links into the dictionary
			string line = "";
			while ((line = reader.ReadLine()) != null)
			{
				linksQueued.Add(line, line);
			}

			logStream.Close();
		}

		/// <summary>
		/// Store the links to disk, overwritting the current file.
		/// </summary>
		static void StoreLinksQueue()
		{
			//Open the log file
			FileStream logStream = File.Open(LINKS_QUEUE_FILENAME, FileMode.Create);

			StreamWriter reader = new StreamWriter(logStream);

			foreach (var v in linksQueued)
				reader.WriteLine(v.Value);

			reader.Close();
		}

		/// <summary>
		/// Loads the list of guilds and their GIDs.
		/// </summary>
		static void LoadGuilds()
		{
			LoadIDList(GUILDS_FILENAME, guilds, ref nextGID);
		}


		static void LoadIDList(string filename, Dictionary<string, int> ids, ref int next)
		{
			//Open the log file
			FileStream logStream;
			try
			{
				logStream = File.OpenRead(filename);
			}
			catch (FileNotFoundException)
			{
				next = 0;
				return;
			}

			StreamReader reader = new StreamReader(logStream);


			//Read all of the lines in the log and extract the links into the dictionary
			string line = "";
			while ((line = reader.ReadLine()) != null)
			{
				int pos = line.LastIndexOf(':');
				if (pos == -1 || pos + 1 >= line.Length)
				{
					Console.WriteLine("Error: Invalid line in " + filename + ".");
					continue;
				}

				int id = Int32.Parse(line.Substring(pos + 1));

				string key = line.Substring(0, pos);

				ids.Add(key, id);

				//Set the nextPID to one more than the highest PID
				if (id > next)
					next = id + 1;
			}

			logStream.Close();
		}

		/// <summary>
		/// Store the dictionary of guilds and their GIDs.
		/// </summary>
		static void StoreGuilds()
		{
			FileStream logStream = File.Open(GUILDS_FILENAME, FileMode.Create);

			StreamWriter reader = new StreamWriter(logStream);

			foreach (var d in guilds)
				reader.WriteLine(d.Key + ":" + d.Value);

			reader.Close();
		}

		/// <summary>
		/// Loads the next WID and PID to use.
		/// </summary>
		static void LoadNextIDs()
		{
			//Open the log file
			FileStream logStream;
			try
			{
				logStream = File.OpenRead(NEXT_IDS_FILENAME);
			}
			catch (FileNotFoundException)
			{
				nextPID = 0;
				nextWID = 0;
				return;
			}

			StreamReader reader = new StreamReader(logStream);


			//Read all of the lines in the log and extract the links into the dictionary
			string line = "";
			while ((line = reader.ReadLine()) != null)
			{
				string[] tokens = line.Split(' ');
				if (tokens.Length == 2)
				{
					if (tokens[0] == "PID")
						nextPID = Int32.Parse(tokens[1]);
					else if (tokens[0] == "WID")
						nextWID = Int32.Parse(tokens[1]);
				}
			}

			reader.Close();
		}

		/// <summary>
		/// Store the next WID and PID.
		/// </summary>
		static void StoreNextIDs()
		{
			FileStream logStream = File.Open(NEXT_IDS_FILENAME, FileMode.Create);

			StreamWriter reader = new StreamWriter(logStream);

			reader.WriteLine("PID " + nextPID);
			reader.WriteLine("WID " + nextWID);

			reader.Close();
		}

		/// <summary>
		/// Pulls an initial seed of characters from the challenge API
		/// </summary>
		static void SeedCharacters()
		{
			string link = BASE_URL + "challenge/tichondrius";

			DataContractJsonSerializer d = new DataContractJsonSerializer(typeof(Challenge));
			Challenge challenge = d.ReadObject(Scrape(link)) as Challenge;

			foreach (var a in challenge.elements)
				foreach (var b in a.groups)
					foreach (var c in b.members)
						if (c.character != null && c.character.realm == "Tichondrius")
						{
							string url = BASE_URL + "character/" + c.character.realm.ToLower() + "/" + c.character.name.ToLower();
							if (!linksQueued.ContainsKey(url))
								linksQueued.Add(url, url);
						}
			StoreLinksQueue();
		}

		/// <summary>
		/// Downloads a character's guild and sets its GID.
		/// </summary>
		/// <param name="character">The character whose guild is downloaded.</param>
		/// <param name="queries">The stream of mysql queries.</param>
		static void ScrapeGuild(Character character, StreamWriter queries)
		{
			string guildPlusRealm = character.guild.name + character.guild.realm;

			//If we have already pulled the guild just set the GID in character
			if (guilds.ContainsKey(guildPlusRealm))
			{
				character.GID = guilds[guildPlusRealm];
				return;
			}
			
			//Otherwise pull the guild
			string link = BASE_URL + "guild/" + character.guild.realm + "/" + character.guild.name;
			DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(Guild));
			Guild guild = serializer.ReadObject(Scrape(link + "?fields=members")) as Guild;

			//Need to error check guild (it could be null)

			//Set the GID
			guild.GID = nextGID++;
			guilds.Add(guildPlusRealm, guild.GID);

			//Print the guild query
			guild.PrintQuery(queries);

			//Finally, set the character's GID
			character.GID = guild.GID;

			//And add the members to the queue
			foreach (Guild.Member m in guild.members)
			{
				//If for some reason the character isn't filled out, continue
				if (m.character == null || m.character.realm == null || m.character.name == null)
					continue;

				string memberLink = BASE_URL + "character/" + m.character.realm.ToLower() + "/" + m.character.name.ToLower()
					+ CHARACTER_FIELDS_URL_END;

				try
				{
					linksQueued.Add(memberLink, memberLink);
				}
				catch (ArgumentException)
				{
					//Skip characters already queued
				}
			}
		}

		static void ScrapeItems(Character character, StreamWriter queries)
		{
			ScrapeItem(character.items.back, queries);
			ScrapeItem(character.items.chest, queries);
			ScrapeItem(character.items.feet, queries);
			ScrapeItem(character.items.finger1, queries);
			ScrapeItem(character.items.finger2, queries);
			ScrapeItem(character.items.hands, queries);
			ScrapeItem(character.items.legs, queries);
			ScrapeItem(character.items.mainHand, queries);
			ScrapeItem(character.items.offHand, queries);
			ScrapeItem(character.items.neck, queries);
			ScrapeItem(character.items.shoulder, queries);
			ScrapeItem(character.items.trinket1, queries);
			ScrapeItem(character.items.trinket2, queries);
			ScrapeItem(character.items.waist, queries);
			ScrapeItem(character.items.wrist, queries);
		}


		static void ScrapeItem(Character.Items.Item characterItem, StreamWriter queries)
		{
			if (characterItem == null)
				return;

			string link = BASE_URL + "item/" + characterItem.IID;

			//If we already scraped the item
			if (scrapedUrls.ContainsKey(link))
				return;

			DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(Item));
			Item item = serializer.ReadObject(Scrape(link)) as Item;

			//Check for null

			item.PrintQuery(queries, ref nextWID);
		}

		static void StoreFailedLink(string link)
		{
			FileStream fileStream = File.Open(FAILED_LINKS_FILENAME, FileMode.Append);

			StreamWriter writer = new StreamWriter(fileStream);

			writer.WriteLine(link);

			writer.Close();
		}

		static void Main(string[] args)
		{
			DateTime start = DateTime.Now;
			TimeSpan duration = new TimeSpan(0, 30, 0);

			//Pick up where we left off
			LoadScrapedUrls();
			LoadLinksQueue();
			//We need a list of GIDs
			LoadGuilds();
			//And we need the current PID and WID
			LoadNextIDs();

			//If we have no links queued start from scratch
			if (linksQueued.Count < 1)
				SeedCharacters();

			//Quit if something went wrong
			if (nextPID == -1 || nextGID == -1)
			{
				Console.WriteLine("Error: Next ID numbers were not read from file");
				return;
			}

			//The stream for the mysql queries that populate the database
			StreamWriter queries = new StreamWriter(File.Open(QUERIES_FILENAME, FileMode.Append));

			//Scrape the talents
			string talentUrl = BASE_URL + "data/talents";
			if (!scrapedUrls.ContainsKey(talentUrl))
			{
				DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(TalentData));
				TalentData talentData = serializer.ReadObject(Scrape(talentUrl)) as TalentData;

				talentData.PrintQuery(queries);
				queries.Flush();
			}

			//Scrape the achievements
			string achievementUrl = BASE_URL + "data/character/achievements";
			if (!scrapedUrls.ContainsKey(achievementUrl))
			{
				DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(AchievementData));
				AchievementData achievementData = serializer.ReadObject(Scrape(achievementUrl)) as AchievementData;

				achievementData.PrintQuery(queries);
				queries.Flush();
			}

			DateTime lastDiskWrite = DateTime.Now;

			//Run for duration time
			while ((DateTime.Now - start) < duration || linksQueued.Count < 1)
			{
				//Every 5 minutes write our queue to disk
				if ((DateTime.Now - lastDiskWrite) > new TimeSpan(0, 5, 0))
				{
					lastDiskWrite = DateTime.Now;
					StoreLinksQueue();
				}

				var enumerator = linksQueued.GetEnumerator();
				enumerator.MoveNext();

				string link = enumerator.Current.Value;

				Console.WriteLine(scrapeCount + ": " + link);

				if (link == null)
				{
					Console.WriteLine("Error: linksQueued contained a null link");
					linksQueued.Remove(enumerator.Current.Key);
					continue;
				}

				//If the link has already been scraped remove it from the queue and continue
				if (scrapedUrls.ContainsValue(link))
				{
					linksQueued.Remove(enumerator.Current.Key);
					continue;
				}

				//If the link is a character
				if (link.StartsWith(BASE_URL + "character/"))
				{
					try
					{
						DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(Character));
						Character character = serializer.ReadObject(Scrape(link)) as Character;

						character.PID = nextPID++;

						//Download the guild and put the GID into character
						ScrapeGuild(character, queries);

						//Print the character as an insert query
						character.PrintQuery(queries);

						//Download the items
						ScrapeItems(character, queries);

						//Lastly, remove the link from the queue
						linksQueued.Remove(enumerator.Current.Key);
					}
					catch (WebException we)
					{
						HttpWebResponse errorResponse = we.Response as HttpWebResponse;
						if (errorResponse.StatusCode == HttpStatusCode.NotFound)
							Console.WriteLine("Error: 404 from " + link + ".");
						else
							Console.WriteLine("Error: WebException from " + link + ".\n" + we.Message);

						StoreFailedLink(enumerator.Current.Key);
						linksQueued.Remove(enumerator.Current.Key);
						continue;
					}
					catch (NullReferenceException)
					{
						Console.WriteLine("Error: NullReferenceException from " + link + ".");
						StoreFailedLink(enumerator.Current.Key);
						linksQueued.Remove(enumerator.Current.Key);
						continue;
					}
					catch (Exception e)
					{
						Console.WriteLine("Error: Something broke while scraping " + link + ".\n" + e.Message);
						StoreFailedLink(enumerator.Current.Key);
						linksQueued.Remove(enumerator.Current.Key);
						continue;
					}
				}
				//If we screwed up print and error and remove the link
				else
				{
					Console.WriteLine("Error: Invalid link: " + link);
					StoreFailedLink(enumerator.Current.Key);
					linksQueued.Remove(enumerator.Current.Key);
					continue;
				}

				queries.Flush();
			}

			queries.Close();

			//Store the final queue once we are done
			StoreLinksQueue();

			//Store the guilds and their GIDs
			StoreGuilds();

			//Store the next WID and PID
			StoreNextIDs();
		}
	}
}