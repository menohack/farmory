﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;

namespace Farmory
{
	[DataContract]
	public class Item
	{
		[DataMember(Name = "armor")]
		int armor;

		[DataMember(Name = "baseArmor")]
		int baseArmor;

		[DataMember(Name = "buyPrice")]
		int buyPrice;

		[DataMember(Name = "containerSlots")]
		int containerSlots;

		[DataMember(Name = "description")]
		string description;

		[DataMember(Name = "disenchantingSkillRank")]
		int disenchantingSkillRank;

		[DataMember(Name = "equippable")]
		bool equippable;

		[DataMember(Name = "hasSockets")]
		bool hasSockets;

		[DataMember(Name = "heroicTooltip")]
		bool heroicTooltip;

		[DataMember(Name = "id")]
		int id;

		[DataMember(Name = "inventoryType")]
		int inventoryType;

		[DataMember(Name = "isAuctionable")]
		bool isAuctionable;

		[DataMember(Name = "itemBind")]
		int itemBind;

		[DataMember(Name = "itemClass")]
		int itemClass;

		[DataMember(Name = "itemLevel")]
		int itemLevel;

		[DataContract]
		public class ItemSource
		{
			[DataMember(Name = "sourceId")]
			public int sourceId;

			[DataMember(Name = "sourceType")]
			public string sourceType;
		}

		[DataMember(Name = "itemSource")]
		ItemSource itemSource;

		[DataMember(Name = "itemSubClass")]
		int itemSubClass;

		[DataMember(Name = "maxCount")]
		int maxCount;

		[DataMember(Name = "maxDurability")]
		int maxDurability;

		[DataMember(Name = "minFactionId")]
		int minFactionId;

		[DataMember(Name = "minReputation")]
		int minReputation;

		[DataMember(Name = "name")]
		string name;

		[DataMember(Name = "nameDescription")]
		string nameDescription;

		[DataMember(Name = "quality")]
		int quality;

		[DataMember(Name = "requiredLevel")]
		int requiredLevel;

		[DataMember(Name = "requiredSkill")]
		int requiredSkill;

		[DataMember(Name = "requiredSkillRank")]
		int requiredSkillRank;

		[DataMember(Name = "sellPrice")]
		int sellPrice;

		[DataMember(Name = "stackable")]
		int stackable;

		[DataMember(Name = "upgradable")]
		bool upgradable;

		[DataContract]
		class WeaponInfo
		{
			[DataContract]
			public class Damage
			{
				[DataMember(Name = "exactMax")]
				public decimal exactMax;

				[DataMember(Name = "exactMin")]
				public decimal exactMin;

				[DataMember(Name = "max")]
				public int max;

				[DataMember(Name = "min")]
				public int min;
			}

			[DataMember(Name = "damage")]
			public Damage damage;

			[DataMember(Name = "dps")]
			public decimal dps;

			[DataMember(Name = "weaponSpeed")]
			public decimal weaponSpeed;
		}

		[DataMember(Name = "weaponInfo")]
		WeaponInfo weaponInfo;

		public void PrintQuery(StreamWriter stream, ref int nextWID)
		{
			//Write the weaponinfo if it is a weapon and set the WeaponID
			if (weaponInfo != null)
				stream.WriteLine("INSERT INTO WeaponInfo VALUES ({0},{1},{2},{3},{4},{5},{6});", nextWID, 
					weaponInfo.damage.exactMax, weaponInfo.damage.exactMin, weaponInfo.damage.max, weaponInfo.damage.min, 
					weaponInfo.dps, weaponInfo.weaponSpeed);
				

			stream.WriteLine("INSERT INTO Items VALUES (" + id + "," + armor + "," + baseArmor + "," + buyPrice + ","
				+ containerSlots + ",\"" + description.Replace("\"", "\\\"") + "\"," + (equippable == true ? 1 : 0) + ","
				+ (hasSockets == true ? 1 : 0) + "," + (heroicTooltip == true ? 1 : 0) + "," + inventoryType + "," 
				+ (isAuctionable == true ? 1 : 0) + "," + itemBind + "," + itemClass + "," + itemLevel + ","
				+ itemSource.sourceId + ",\"" + itemSource.sourceType + "\"," + itemSubClass + ",\""
				+ name.Replace("\"", "\\\"") + "\"," + quality + "," + requiredLevel + "," + requiredSkill + "," 
				+ requiredSkillRank + "," + sellPrice + "," + stackable + "," + (upgradable == true ? 1 : 0) + "," 
				+ (weaponInfo == null ? -1 : nextWID) + ");");

			if (weaponInfo != null)
				nextWID++;
		}

		//We should swap itemClass with the string instead of the int
	}
}
