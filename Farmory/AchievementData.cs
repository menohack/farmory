﻿using System.Runtime.Serialization;
using System.Collections.Generic;
using System.IO;

namespace Farmory
{
	[DataContract]
	public class AchievementData
	{
		[DataContract]
		public class Category
		{
			[DataMember(Name = "id")]
			public int id;

			[DataMember(Name = "categories")]
			public List<Category> categories;

			[DataContract]
			public class Achievement
			{
				[DataMember(Name = "id")]
				public int id;

				[DataMember(Name = "title")]
				public string title;

				[DataMember(Name = "points")]
				public int points;

				[DataMember(Name = "description")]
				public string description;

				[DataContract]
				public class Item
				{
					[DataMember(Name = "id")]
					int id;

					[DataMember(Name = "name")]
					string name;

					[DataMember(Name = "icon")]
					string icon;

					[DataMember(Name = "quality")]
					int quality;

					[DataMember(Name = "itemLevel")]
					int itemLevel;

					//Skipped tooltipParams

					//Skipped stats

					[DataMember(Name = "armor")]
					int armor;
				}

				[DataMember(Name = "rewardItems")]
				public List<Item> rewardItems;

				[DataMember(Name = "icon")]
				public string icon;

				[DataContract]
				public class Criteria
				{
					[DataMember(Name = "id")]
					public int id;

					[DataMember(Name = "description")]
					public string description;

					[DataMember(Name = "orderIndex")]
					public int orderIndex;

					[DataMember(Name = "max")]
					public int max;
				}

				[DataMember(Name = "criteria")]
				public List<Criteria> criteria;

				[DataMember(Name = "accountWide")]
				public bool accountWide;

				[DataMember(Name = "factionId")]
				public int factionId;
			}

			[DataMember(Name = "achievements")]
			public List<Achievement> achievements;

			[DataMember(Name = "name")]
			public string name;
		}

		[DataMember(Name = "achievements")]
		List<Category> achievements;

		public void PrintQuery(StreamWriter stream)
		{
			if (achievements != null)
				foreach (Category c in achievements)
					PrintCategory(stream, c);
		}

		/// <summary>
		/// Recursively print categories. Categories may contain categories.
		/// </summary>
		/// <param name="stream">The stream to which queries are printed.</param>
		/// <param name="category">The category to print.</param>
		private static void PrintCategory(StreamWriter stream, Category category)
		{
			if (category.achievements != null)
				foreach (Category.Achievement a in category.achievements)
					PrintAchievement(stream, a);

			if (category.categories != null)
				foreach (Category c in category.categories)
					PrintCategory(stream, c);
		}

		/// <summary>
		/// Prints an achievement insertion query.
		/// </summary>
		/// <param name="stream">The stream to which queries are printed.</param>
		/// <param name="a">The achievement to print.</param>
		private static void PrintAchievement(StreamWriter stream, Category.Achievement a)
		{
			//Print the achievement, replacing double quotes with the escape character and the quote for mysql
			stream.WriteLine("INSERT INTO Achievements VALUES (" + a.id + "," + a.points + ",\"" + a.title.Replace("\"", "\\\"") + "\",\""
									+ a.description.Replace("\"", "\\\"") + "\");");

			//Print the criteria
			if (a.criteria != null)
				foreach (Category.Achievement.Criteria c in a.criteria)
					stream.WriteLine("INSERT INTO AchievementPrerequisites VALUES (" + c.id + "," + a.id + ");");

			//We could print the reward items here but we don't have a table for it
		}
	}
}
